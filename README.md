Once the git repo has been downloaded, open VSCode and open the project folder. Open a new terminal and type "cd forum", then type "npm install". Let the code do its thing until it doesn't run anymore, then type "npm run start" and the development server will open. Once there, feel free to 

-choose the topics and categories using the dropdown menus

-add a post by typing in the textarea near the bottom of the page and then pressing the submit button

-search through the selected posts by typing in the search bar

-use the admin options on the left

-like,dislike and delete a post through the images on each post