import Header from "./Components/Header";
import Footer from "./Components/Footer";
import Main from "./Components/Main";
import { useState } from "react";
import { HeaderContext } from "./Context/HeaderContext";

function App() {
  const [query, setQuery] = useState("");
  return (
    //Here we need to use Context API to pass data from the siblings Header 
    //and Main in order for Forum.js to recieve the input of the input text search bar
    <div>
      <HeaderContext.Provider value={{query, setQuery}}>
        <Header/> 
        <Main/>
      </HeaderContext.Provider>
      <Footer/>
    </div>
  );
}

export default App;
