import { useState, useEffect } from "react";

/**
 * Fetches json from a url, returns a response json
 * @param {String} url 
 * @returns {Promise}
 */
const useFetchdata = (url)=>{
    const [data,setData]=useState(null);
    useEffect(()=>{
        // fetch from url
        fetch(url).then(response=>{
            if(response.ok){ // get the json ONLY if it exists
                return response.json();
            }
            else{ // reject if its not 200
                Promise.reject(`${response.status}:${response.statusText}`);
            }
        }).then((response)=>{ // set data
            setData(response);
        }).catch(error=>console.log("Cannot retrieve json: "+error));
    },[url])
    return [data,setData]; // return the state
}

/**
 * Gets the category by id
 * @returns {Object}
 */
 const getCategory = (postData, categoryId)=>{
    return postData.categories[categoryId-1];
}

/**
 * Gets the topic by id
 * @returns {Object}
 */
 const getTopic = (postData, categoryId, topicId)=>{
    return postData.categories[categoryId-1].topicList[topicId-1];
}

/**
 * Gets the next category id that is available
 * @returns {Number}
 */
const getNextCategory = (postData)=>{
    postData.categorySeq++;
    return postData.categorySeq;
}

/**
 * Gets the next topic id that is available
 * @returns {Number}
 */
 const getNextTopic = (postData, category)=>{
    let categoryData =  getCategory(postData,category)
    categoryData.topicSeq++;
    return categoryData.topicSeq;
}

export {useFetchdata, getCategory, getTopic, getNextCategory, getNextTopic};