import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";
import { getNextCategory } from "../../Data";

const CreateCategoryButton = (props)=>{
    const {postData,setPostData} = useContext(DataContext);
    const createCategory = ()=>{
        // prompt admin to enter a name
        let categoryName = prompt("Enter the category name");
        let blank = /^\s+$/
        // exit if the name is null or it its blank
        if(!categoryName){
            return;
        }
        if(blank.test(categoryName)){
            return;
        }
        // create category data
        let id = getNextCategory(postData);
        let categoryData = {
            id: id,
            name: `Category ${id} - ${categoryName}`,
            author: "admin",
            nberTopics: 0,
            topicSeq: 0,
            topicList:[]
        };
        // add to data
        postData.categories.push(categoryData);
        setPostData(JSON.parse(JSON.stringify(postData)));
    }
    return (
    <button onClick={createCategory} className="admin-button">
        Create Category
    </button>        
    )
}

export default CreateCategoryButton;