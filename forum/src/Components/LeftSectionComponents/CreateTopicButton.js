import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";
import { TopicContext } from "../../Context/TopicContext";
import { getNextTopic, getCategory } from "../../Data";

const CreateTopicButton = (props)=>{
    const {postData,setPostData} = useContext(DataContext);
    const {category} = useContext(TopicContext);
    const createTopic = ()=>{
        let categoryData = getCategory(postData,category);
        // prompt admin to enter a name
        let topicName = prompt("Enter the topic name");
        let blank = /^\s+$/;
        // exit if the name is null or it its blank
        if(!topicName){
            return;
        }
        if(blank.test(topicName)){
            return;
        }
        // create category data
        let id = getNextTopic(postData,category);
        let topicData = {
            id: id,
            topic_title: `${topicName}`,
            nberPost: 0,
            status: "ongoing",
            listPosts:[]
        };
        // add to data
        categoryData.topicList.push(topicData);
        setPostData(JSON.parse(JSON.stringify(postData)));
    }
    return (
    <button onClick={createTopic} className="admin-button">
        Create Topic
    </button>        
    )
}

export default CreateTopicButton;