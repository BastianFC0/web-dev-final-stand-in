import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";
import { TopicContext } from "../../Context/TopicContext";
import { getCategory } from "../../Data";
const DeleteTopicButton = (props)=>{
    const {postData,setPostData} = useContext(DataContext);
    const {category,topic,setTopic} = useContext(TopicContext);
    const closeTopic = ()=>{
        // ask to close
        if(window.confirm("Delete the topic?")){       
            let categoryData = getCategory(postData,category); 
            categoryData.topicList.splice(topic-1,1);
            categoryData.nberTopics--;
            setTopic(1);

            // update data
            setPostData(JSON.parse(JSON.stringify(postData)));
        }
    }
    return (
        <button onClick={closeTopic} className="admin-button">
            Delete Topic
        </button>        
    );
}
export default DeleteTopicButton;