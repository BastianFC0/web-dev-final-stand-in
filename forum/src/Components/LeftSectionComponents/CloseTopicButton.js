import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";
import { TopicContext } from "../../Context/TopicContext";
import { getTopic } from "../../Data";

const CloseTopicButton = (props)=>{
    const {postData,setPostData} = useContext(DataContext);
    const {category,topic} = useContext(TopicContext);
    const closeTopic = ()=>{
        // ask to close
        if(window.confirm("Close the topic?")){            
            let topicData = getTopic(postData,category,topic);
            topicData.status = "closed";

            // update data
            setPostData(JSON.parse(JSON.stringify(postData)));
        }
    }
    return (
        <button onClick={closeTopic} className="admin-button">
            Close Topic
        </button>        
    );
}
export default CloseTopicButton;