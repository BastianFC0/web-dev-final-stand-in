import Forum from "./MainSectionComponents/Forum";
import TopicSearch from "./MainSectionComponents/TopicSearch";
import PostSubmit from "./MainSectionComponents/PostSubmit";
import React from "react";
import { useContext } from "react";
import { DataContext } from "../Context/DataContext";
import { TopicContext } from "../Context/TopicContext";
import { getCategory, getTopic } from "../Data";

const MainColumn = (props)=>{
    const {category,topic}= useContext(TopicContext);    
    const {postData,setPostData} = useContext(DataContext);
    // wait for the data to load
    if(postData==null){
        return <p>Loading...</p>
    }
    // get the category
    let categoryData = getCategory(postData,category);
    let topicData = getTopic(postData,category,topic) || {};
    let posts = topicData.listPosts || [];
    // deletes a post
    const deletePost = (postId)=>{
        // get the index of the post and set it to undefined
        posts.splice(postId-1,1);        
        setPostData(JSON.parse(JSON.stringify(postData)));
    }
    return ( 
        <section id="section_Main_Column">   
            
            <TopicSearch data={postData}/>  
            <section id="section_Forum">
                <Forum posts={posts} deletePost={deletePost} category={category} topic={topic} />
                <PostSubmit posts={posts}/>
            </section>
        </section>
     );
}

 
export default MainColumn;