import RankedTopics from "./RightSectionComponents/RankedTopics";
import RecentPosts from "./RightSectionComponents/RecentPosts";
import StatsPosts from "./RightSectionComponents/StatsPosts";
const RightColumn = () => {
    return ( 
        <section id="section_Right_Column">
            <RankedTopics/>
            <RecentPosts/>
            <StatsPosts/>
        </section>
     );
}
 
export default RightColumn;