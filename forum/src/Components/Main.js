import LeftColumn from "./LeftColumn";
import MainColumn from "./MainColumn";
import RightColumn from "./RightColumn";
import { useFetchdata } from "../Data";
import { DataContext } from "../Context/DataContext";
import { useState } from "react";
import { TopicContext } from "../Context/TopicContext";

const Main = () => {
    // init both fetch data hooks
    const [postData,setPostData] = useFetchdata("./json/forum.json")
    const [userData,setUserData] = useFetchdata("./json/users.json")
    // init category and topic states
    const [category,setCategory]=useState(1);
    const [topic,setTopic]=useState(1)
    return ( 
        <section id="section_Main_Section">
            <TopicContext.Provider value={{category,setCategory,topic,setTopic}}>
                <DataContext.Provider value={{postData,userData,setPostData,setUserData}}>
                    <LeftColumn/>
                    <MainColumn/>
                    <RightColumn/>
                </DataContext.Provider>
            </TopicContext.Provider>
        </section>
     );
}
 
export default Main;