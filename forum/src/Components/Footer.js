const Footer = () => {
    return ( 
        <footer>
            <h1>Project 2 - Using React</h1>
            <p id="p_Student_Names">Bastian Fernandez Cortez/2041556,
            Nicoleta Sarghi/2039804
            copyright Fall 2021</p>
        </footer>
     );
}
 
export default Footer;