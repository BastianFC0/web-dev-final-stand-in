import {useContext } from "react";
import { HeaderContext } from "../Context/HeaderContext";

const Header = () => {
    //HeaderContext that allows the use of setQuery
    const {setQuery} = useContext(HeaderContext);
    //The event onChange sets the query
    return ( 
        <header>
            <h1>Pseudo Forum</h1>
            <input type="text" id="input_Header_Search_Bar" onChange={(event) =>{
                setQuery(event.target.value);
            }}/>
        </header>
     );
}
 
export default Header;