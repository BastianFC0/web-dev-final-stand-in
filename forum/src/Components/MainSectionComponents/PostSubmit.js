import { useState, useContext } from "react";
import { DataContext } from "../../Context/DataContext";
import { TopicContext } from "../../Context/TopicContext";
import { getTopic } from "../../Data";
const PostSubmit = (props)=>{
    const {postData,setPostData} = useContext(DataContext);
    const {category,topic} = useContext(TopicContext);
    const [text,setText] = useState("")
    let posts = props.posts 
    const updateText = (event)=>{
        setText(event.target.value)
    }
    const createPost = (event)=>{
        let currentDate = new Date();
        // get the formatted dates
        let formattedDate = currentDate.toLocaleDateString('en-CA');
        let formattedTime = currentDate.toLocaleTimeString('en-CA',{hour: '2-digit', minute:'2-digit', hour12: false});
        // create post obj
        let newPost = {
            id:posts.length+1,
            topic_id:`cat${category+1}_topic${topic+1}_post1`,
            parentId:`topic${topic+1}`,
            author:"admin",
            date:`${formattedDate} ${formattedTime}`,
            text:text,
            rate:0,
            likes:0,
            replies:0,
        };        
        posts.push(newPost);
        getTopic(postData,category,topic).nberPost++;
        setPostData(JSON.parse(JSON.stringify(postData)));
    } 
    let boxData;
    let topicData = getTopic(postData,category,topic);
    if(!topicData){
        return (
            <section id="section_Submission">
                <p>There are no topics in this category.</p>
            </section>
        );
    }
    // if the topic is closed then remove the box
    if(topicData.status==="closed"){
        boxData = (
        <section id="section_Submission">
            <p>This topic is closed.</p>
        </section>);        
    }else{
        boxData = (
        <section id="section_Submission">
            <section id="section_Submission_Text">
                <textarea onChange={updateText} name="textarea" maxcols="30" maxrows="10"></textarea>
            </section>
            <button onClick={()=>createPost(text)} id="button_Submit">Submit Post</button> 
        </section>
        );
    }
    return boxData 
}

export default PostSubmit