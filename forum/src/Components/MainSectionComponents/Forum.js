import ForumPost from "./ForumPost"
import { HeaderContext } from "../../Context/HeaderContext";
import { useContext } from "react";

const Forum = (props) => {
    let deleteFunction = (post)=>{
        let postId = post.id
        props.deletePost(postId)
    }   
    //HeaderContext that allows us to get the value of the input
    const {query} = useContext(HeaderContext);
    let posts = props.posts

    let isFiltering = !(query==="")
    if (isFiltering){ 
        //If the search bar is active, it shall search for any corresponding entry
        let regex = new RegExp(query);
        posts = props.posts.filter(post=>regex.test(post.text)
        || regex.test(post.author) || regex.test(post.date) ||
        regex.test(post.likes) || regex.test(post.like))
    }
    return( 
        <nav id="nav_Forum">
            {posts.map(post=>{
                let key = `category${props.category+1}_topic${props.topic+1}_id${post.id}`
                return <ForumPost key={key} post={post} delete={deleteFunction}/>                
            })}
        </nav>
    );
};


 
export default Forum;