import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";
import { TopicContext } from "../../Context/TopicContext";
import { getCategory } from "../../Data";

const CategorySelect = (props)=>{
    const {setTopic} = useContext(TopicContext);
    const {setCategory} = useContext(TopicContext);

    let switchCategory = (event)=>{
        setCategory(event.target.value)
        setTopic(1) // reset topic
    }
    return(
    <select onChange={switchCategory} name="select_Topic">
        {props.categories.map(categoryData=><option key={categoryData.id} value={categoryData.id}>{categoryData.name}</option>)}
    </select>        
    )
}

const TopicSelect = (props)=>{
    const {category,setTopic} = useContext(TopicContext);
    const {postData} = useContext(DataContext);
    const switchTopic = (event)=>{
        setTopic(event.target.value)
    }
    let categoryData = getCategory(postData,category)
    let topicList = categoryData.topicList
    return (
    <select onChange={switchTopic} name="select_Topic">  
        {topicList.map(topicData=><option key={`${categoryData.id}_${topicData.id}`} value={topicData.id}>{topicData.topic_title}</option>)}              
    </select>        
    )
}

const TopicSearch = (props) => {
    // get the data and category from the props
    const {postData} = useContext(DataContext);
   
    return ( 
    <section id="section_Topic_Search">
        <section className="section_Topic_Holder">
            <h3>Category</h3>
            <CategorySelect categories = {postData.categories}/>
        </section>
        <section className="section_Topic_Holder">
            <h3>Topics</h3>
            <TopicSelect categories = {postData.categories}/>
        </section>
    </section>
     );
}
 
export default TopicSearch;