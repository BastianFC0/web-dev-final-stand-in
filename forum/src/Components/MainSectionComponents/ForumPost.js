import { useState } from "react"
import dislikeIcon from "../../Icons/thumbs-down-regular.svg"
import likeIcon from "../../Icons/thumbs-up-regular.svg"
import trashIcon from "../../Icons/trash-alt-regular.svg"
const HasLiked = Symbol("Liked") 
const HasDisliked = Symbol("Disliked") 
const Neutral = Symbol("Neutral")

const ForumPost = (props) =>{
    const [postStatus,setPostStatus]=useState(Neutral);
    // first define functions for setting the post state
    const switchState = (newState)=>{
        newState = postStatus === newState? Neutral : newState
        setPostStatus(newState)
    }
    const likePost = ()=>{
        switchState(HasLiked)
    };
    const dislikePost = ()=>{
        switchState(HasDisliked)
    }
    // create classnames for showing selected/deselected
    let likeClass = postStatus===HasLiked? "selected" : "";
    let dislikeClass  = postStatus===HasDisliked? "selected" : "";
    // determine the variables based on the state
    let likes = props.post.likes || 0;
    likes = postStatus===HasLiked? likes+1 : likes;
    // dislikes are determined based on the ratio
    let dislikes = Math.floor(likes/(props.post.rate))|| 0
    dislikes = isFinite(dislikes) ? dislikes : 0
    dislikes = postStatus === HasDisliked? dislikes+1 : dislikes

    // draw the frame
    return(
    <section className="section_Posts">
        <section className="post_Top_Section">
            <p>{props.post.text}</p>
            <button onClick={likePost}>
                <img className={likeClass} alt={"like"} src={likeIcon}></img>
            </button>
            <button onClick={dislikePost}>
                <img className={dislikeClass} alt={"dislike"} src={dislikeIcon}></img>
            </button>
        </section>
        <section className="post_Bottom_Section">
            <section>{props.post.author}</section>
            <section>{props.post.date}</section>  
            <section>{likes} likes</section>
            <section>{dislikes} dislikes</section>  
            <button onClick={()=>props.delete(props.post)}>
                <img alt={"delete"} src={trashIcon}></img>
            </button> 
        </section>
    </section>);
}

export default ForumPost;