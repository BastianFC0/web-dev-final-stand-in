import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";

const StatsPosts = () => {    
    const {userData} = useContext(DataContext);
    if(userData==null){
        return <p> Loading... </p>
    }
    let users = userData.users.sort((a,b)=>b.nberPosts-a.nberPosts);

    return ( 
        
        <nav id="nav_Stats">
            <section className="section_Title_Box">
                <h3 className="h3_Title">Stats Posts</h3>
            </section>
            <section className="section_Display_Several">
                {users.map((post)=>
                    <section key={post.user_id} className="section_Display_Unit">
                        <p>{post.user_id}</p>
                        <p>{post.nberPosts}</p>
                    </section>
                )}          

            </section>
        </nav>
     );
}
 
export default StatsPosts;