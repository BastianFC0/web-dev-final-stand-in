import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";
/**
 * 
 * @returns either loading or the Recent Post box
 */
const RecentPosts = () => {
    const {postData} = useContext(DataContext);
    if(postData==null){
        return <p> Loading... </p>
    }
    //Here we make an array with posts that have dates that are less than a month old
    const currentDate = new Date();
    currentDate.setDate(currentDate.getDate()-30);
    let orderedPosts = [];
    let arrayHolder;
    for( let i = 0; i < postData.categories.length; i++){
        for(let j = 0; j < postData.categories[i].topicList.length; j++){
            for(let k = 0; k < postData.categories[i].topicList[j].listPosts.length; k++){
                arrayHolder = postData.categories[i].topicList[j].listPosts[k];
                if(Date.parse(arrayHolder.date) > currentDate){
                orderedPosts.push(arrayHolder);
                }
            }
        }
    }
    function compare(a,b){
        if(a.date < b.date){
            return 1;
        }
        if(a.date > b.date){
            return -1;
        }
            return 0;
     }
        orderedPosts.sort(compare);
    return ( 
        <nav id="nav_Recent">
            <section className="section_Title_Box">
                <h3 className="h3_Title">Recent Posts</h3>
            
            </section>
            <section className="section_Display_First">
                <p>Author</p>
                <p id="rank">Rank</p>
                <p>Time</p>
            </section>
            <section className="section_Display_Several">
                {orderedPosts.map((post) =>
                    <section className="section_Display_Unit">
                        <p>{post.author}</p>
                        <p>{post.rate}</p>
                        <p>{post.date}</p>
                    </section>
                )}
            </section>
        </nav>
     );
}
 
export default RecentPosts;