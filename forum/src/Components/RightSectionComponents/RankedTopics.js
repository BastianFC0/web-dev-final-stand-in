import { useContext } from "react";
import { DataContext } from "../../Context/DataContext";
/**
 * 
 * @returns either loading or the Ranked Topics box
 */
const RankedTopics = () => {
    const {postData} = useContext(DataContext);
    if(postData==null){
        return <p> Loading... </p>
    }
    //Here we create an array that holds the topicLists to be used
    let arrayTopic = [];
    for( let i = 0; i < postData.categories.length; i++){
        for(let j = 0; j < postData.categories[i].topicList.length; j++){
            arrayTopic.push(postData.categories[i].topicList[j]);
        }
    }
    arrayTopic.sort((a,b)=> b.nberPost-a.nberPost);
    return ( 
        <nav id="nav_Ranked">
            <section className="section_Title_Box">
                <h3 className="h3_Title">Ranked Topics</h3>
            </section>
            <section className="section_Display_First">
                <p>Title</p>
                <p>Post Number</p>
                <p>Status</p>
            </section>
            <section className="section_Display_Several">
                {arrayTopic.map((post)=>
                    <section className="section_Display_Unit">
                        <p id = "title">{post.topic_title}</p>
                        <p>{post.nberPost}</p>
                        <p>{post.status}</p>
                    </section>
                )}
        </section>
        </nav>
     );
}
 
export default RankedTopics;