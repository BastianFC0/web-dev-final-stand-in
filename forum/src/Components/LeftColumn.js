import CreateCategoryButton from "./LeftSectionComponents/CreateCategoryButton";
import CreateTopicButton from "./LeftSectionComponents/CreateTopicButton";
import CloseTopicButton from "./LeftSectionComponents/CloseTopicButton";
import DeleteTopicButton from "./LeftSectionComponents/DeleteTopicButton";
const LeftColumn = (props) => {    
    return ( 
        <section id="section_Left_Column">
            <section id="section_Admin_Box">
                <section>
                    Admin
                </section>
                <CreateCategoryButton/>
                <CreateTopicButton/>
                <CloseTopicButton/>
                <DeleteTopicButton/>
            </section>
        </section>
     );
}
 
export default LeftColumn;